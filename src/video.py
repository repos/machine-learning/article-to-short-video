import math
from PIL import Image
import numpy as np
import cv2
import os

import moviepy.editor as mp
from moviepy.video.compositing.CompositeVideoClip import CompositeVideoClip
from moviepy.audio.AudioClip import CompositeAudioClip
from moviepy.audio.io.AudioFileClip import AudioFileClip
from moviepy.audio.fx.all import volumex, audio_loop, audio_fadeout

import utils


def zoom_in_effect(clip, zoom_ratio=0.02):
    """
    This function creates a zoom-in effect on a video clip.

    Parameters:
    clip (VideoClip): The video clip to apply the effect to.
    zoom_ratio (float): The zoom ratio to apply at each frame.

    Returns:
    VideoClip: The video clip with the zoom-in effect applied.
    """

    def effect(get_frame, t):
        """
        This function applies the zoom-in effect to each frame of the video clip.

        Parameters:
        get_frame (function): A function that returns the frame at a given time.
        t (float): The time of the frame to apply the effect to.

        Returns:
        ndarray: The frame with the zoom-in effect applied.
        """
        # Convert the frame to an Image object
        img = Image.fromarray(get_frame(t))

        # Get the size of the original frame
        base_size = img.size

        # Calculate the new size of the frame based on the zoom ratio
        new_size = [
            math.ceil(img.size[0] * (1 + (zoom_ratio * t))),
            math.ceil(img.size[1] * (1 + (zoom_ratio * t))),
        ]

        # The new dimensions must be even.
        new_size[0] = new_size[0] + (new_size[0] % 2)
        new_size[1] = new_size[1] + (new_size[1] % 2)

        # Resize the image to the new size
        img = img.resize(new_size, Image.Resampling.LANCZOS)

        # Calculate the crop coordinates to center the image
        x = math.ceil((new_size[0] - base_size[0]) / 2)
        y = math.ceil((new_size[1] - base_size[1]) / 2)

        # Crop the image to the original size
        img = img.crop([x, y, new_size[0] - x, new_size[1] - y]).resize(
            base_size, Image.Resampling.LANCZOS
        )

        # Convert the image back to a numpy array
        result = np.array(img)

        # Close the Image object to free up memory
        img.close()

        return result

    # Apply the effect to the video clip and return the result
    return clip.fl(effect)


def Zoom(clip, mode="in", position="center", speed=1):
    fps = clip.fps
    duration = clip.duration
    total_frames = int(duration * fps)

    def main(getframe, t):
        frame = getframe(t)
        h, w = frame.shape[:2]
        i = t * fps
        if mode == "out":
            i = total_frames - i
        zoom = 1 + (i * ((0.1 * speed) / total_frames))
        positions = {
            "center": [(w - (w * zoom)) / 2, (h - (h * zoom)) / 2],
            "left": [0, (h - (h * zoom)) / 2],
            "right": [(w - (w * zoom)), (h - (h * zoom)) / 2],
            "top": [(w - (w * zoom)) / 2, 0],
            "topleft": [0, 0],
            "topright": [(w - (w * zoom)), 0],
            "bottom": [(w - (w * zoom)) / 2, (h - (h * zoom))],
            "bottomleft": [0, (h - (h * zoom))],
            "bottomright": [(w - (w * zoom)), (h - (h * zoom))],
        }
        tx, ty = positions[position]
        M = np.array([[zoom, 0, tx], [0, zoom, ty]])
        frame = cv2.warpAffine(frame, M, (w, h))
        return frame

    return clip.fl(main)


def prepare_background_music(
    audio_file_path, video_duration, outro_duration, volume=0.25, fadeout_duration=4
):
    bg_music = AudioFileClip(audio_file_path)
    bg_music = audio_loop(bg_music, duration=video_duration - outro_duration)
    bg_music = volumex(bg_music, volume)
    bg_music = audio_fadeout(bg_music, fadeout_duration)
    return bg_music


def main(article):
    config = utils.load_config()
    zoom_speed = config["video_zoom_speed"]
    fps = config["video_fps"]
    output_folder = config["output_folder"]
    output_file = os.path.join(output_folder, article + ".mp4")

    asset_list = utils.load_asset_file()

    ##########################################################
    ######### This sections assembles the main video #########
    ##########################################################

    slides = []

    # for each item in the json file
    for i, scene in enumerate(asset_list):
        audio = AudioFileClip(scene["speech_file"])
        audio = audio.subclip(0, audio.duration - 0.2)
        scene["speech_file"] = audio

        # Combine with image and add pause at the end (optional)
        image_clip = mp.ImageClip(scene["image"], duration=audio.duration)
        video_clip = image_clip.set_audio(audio)

        slides.append(
            video_clip.set_fps(fps)
            .set_duration(scene["speech_duration"] + 0.3)
            .set_audio(scene["speech_file"])
        )

        slides[i] = Zoom(slides[i], mode="out", position="center", speed=zoom_speed)

        if len(scene["image_caption"]) > 0:
            text = scene["image_caption"]
            video_frame_width = slides[
                i
            ].w  # Obtain the width of the current video frame
            # Adjusting the size for text wrapping and leaving margins, also considering padding
            text_clip_size = (
                video_frame_width - 100 - 2 * 15,
                None,
            )  # Subtracting 2*5 for 5px padding on each side
            credit = mp.TextClip(
                text,
                fontsize=40,
                color="RGB(255, 255, 255, 0.7)",
                font="Helvetica-Bold",
                method="caption",
                align="center",
                size=text_clip_size,
            )
            # Adjusting the background clip size to include padding
            bg_clip_size = (
                credit.size[0] + 2 * 15,
                credit.size[1] + 2 * 15,
            )  # Adding 2*5 to include 5px padding on each side
            bg_clip = mp.ColorClip(
                size=bg_clip_size, color=(0, 0, 0), duration=credit.duration
            ).set_opacity(0.4)
            # Positioning the text clip with padding inside the background clip
            credit = CompositeVideoClip(
                [
                    bg_clip.set_position("center"),
                    credit.set_position(("center", 15)),
                ]  # 5px padding from the top
            ).set_duration(scene["speech_duration"])
            # Position the credit clip at the bottom of the slide
            slides[i] = CompositeVideoClip(
                [
                    slides[i],
                    credit.set_position(("center", slides[i].h - credit.h - 200)),
                ]
            )
    #####################################################
    ######### This sections makes outro segment #########
    #####################################################

    ending_clip = AudioFileClip("static/wikimedia_sonic_logo_short.mp3")

    slides.append(
        mp.ImageClip("static/wikimedia_screen_logo.png")
        .set_fps(fps)
        .set_duration(config["outro_duration"])
        .set_audio(ending_clip)
    )

    if len(scene["image_artist"]) > 0:
        # Generate a text clip with text wrapping
        credit_text = f"Image credit: {scene['image_artist']}\nMusic credit: Scott Buckley\nWikipedia is made by humans, this video was made with AI."
    else:
        credit_text = "Music credit: Scott Buckle\nWikipedia is made by humans, this video was made with AI."
    # Generate a text clip
    # Assuming 'w' is the width of the video frame, we need to define or obtain it
    video_frame_width = slides[i].w  # Obtain the width of the current video frame
    credit = mp.TextClip(
        credit_text,
        fontsize=30,
        color="RGB(106, 106, 106, 0)",
        font="New-York-Black",
        method="caption",  # This method enables text wrapping
        align="center",  # Align text to the center
        size=(
            video_frame_width - 100,  # Adjust width for text wrapping, leaving margins
            None,  # Let height adjust dynamically
        ),
    )

    # Generate a background clip for the text
    bg_clip = mp.ColorClip(
        size=credit.size, color=(0, 0, 0), duration=credit.duration
    ).set_opacity(0)
    # Composite the text clip on top of the background clip
    credit = CompositeVideoClip(
        [bg_clip.set_position("center"), credit.set_position("center")]
    ).set_duration(config["outro_duration"])
    # Position the credit clip at the bottom of the slide
    slides[-1] = CompositeVideoClip(
        [
            slides[-1],
            credit.set_position(("center", slides[i].h - credit.h - 200)),
        ],
    )

    #####################################################
    ######### This sections assembles the video #########
    #####################################################

    video = mp.concatenate_videoclips(slides, "compose", bg_color=None, padding=0)

    bg_music = prepare_background_music(
        "static/Scott_Buckley_-_Aurora.mp3", video.duration, config["outro_duration"]
    )
    # get the audio from the video
    audio = video.audio
    # layer in another audio clip on top
    audio = CompositeAudioClip([audio, bg_music])
    # set the audio of the video to the new composite audio
    video.audio = audio

    video.write_videofile(output_file)
