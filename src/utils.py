"""
Contains utility functions for the library.
"""

import json
from pathlib import Path


def load_config() -> dict:
    """Loads the configuration file"""

    # Note: config.json is hardcoded
    with open("config.json", encoding="utf-8") as f:
        config = json.load(f)
    return config


def load_asset_file() -> dict:
    """Loads the asset file containing information about the images and audio
    files to be used in the video."""

    config = load_config()
    asset_folder = Path(config["asset_folder"])
    asset_file = asset_folder / config["asset_file"]

    with open(asset_file, encoding="utf-8") as f:
        asset_file = json.load(f)
    return asset_file


def save_asset_file(asset_data) -> None:
    """Saves the asset file containing information about the images and audio
    files to be used in the video."""

    config = load_config()
    asset_folder = Path(config["asset_folder"])
    asset_file = asset_folder / config["asset_file"]

    with open(asset_file, "w", encoding="utf-8") as f:
        json.dump(asset_data, f)
