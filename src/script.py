"""Script to generate a summary of a text using OpenAI's GPT-4 model."""

import os

import openai
from dotenv import load_dotenv

import utils

# Load environment variables from .env file
load_dotenv()


def create_prompt(asset_text, asset_title, script_sentences, language):
    """Creates a prompt for the GPT-4 model."""

    prompt = f"""{asset_text}
        ----------------------------------------
        The text above is a Wikipedia article about {asset_title}.

        I am creating a short TikTok or YouTube Shorts video about the {asset_title}.

        I need you to write {script_sentences} sentences for the audio
        that will be read by a narrator. The audio should highlight one interesting fact.
        No yapping. No run on sentences. Be as short as possible. Invite people to come to Wikipedia
        to learn more.

        You have to be neutral and balanced. Something isn't "incredible" or "amazing".
        Don't hype up someone or something.

        The style of the video is upbeat, educational, neutral point of view,
        and high energy. I want you to output just the words that will be said
        by the narrator. No emojis, no scene direction, no timestamps.

        DON'T SAY ANYTHING OTHER THAN WHAT THE NARRATOR SHOULD SAY. NEVER SAY
        "TEXT" OR "SCRIPT" OR ANYTHING OTHER THAN WHAT THE NARRATOR SHOULD SAY.

        The goal is to pique people's interest into {asset_title} to talk about
        the absolute most interesting things.

        Write this script in the language of {language}.wikipedia.org
        """

    return prompt


def create_response(prompt, model):
    """Creates a response from the GPT-4 model."""

    openai.api_key = os.environ["OPENAI_API_KEY"]
    answer = openai.chat.completions.create(
        model=model,
        messages=[
            {
                "role": "system",
                "content": "You are a helpful assistant designed to output JSON.",
            },
            {"role": "user", "content": f"{prompt}"},
        ],
    )
    return answer


def parse_response(response):
    return response.choices[0].message.content.strip()


def main():
    """Generates a script for each asset in the asset list."""

    asset_list = utils.load_asset_file()
    config = utils.load_config()
    n = config["script_sentences"]

    for asset in asset_list:
        prompt = create_prompt(
            asset_text=asset["full_text"],
            asset_title=asset["title"],
            script_sentences=n,
            language=config["wiki_language"],
        )
        response = create_response(prompt, model=config["model"])
        script = parse_response(response)
        asset["script"] = script

    utils.save_asset_file(asset_list)
