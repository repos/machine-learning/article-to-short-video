import wikipedia
from bs4 import BeautifulSoup
import requests
import html
import utils


def download_image(base_url, image_page, output_filename, folder, user_agent):
    headers = {"User-Agent": user_agent}

    # Use requests to download the HTML of a page
    html = requests.get(base_url + image_page, timeout=10).text

    soup = BeautifulSoup(html, "html.parser")

    # Attempt to find the text that says "Original file" and get the parent href
    original_file_element = soup.find(string="Original file")
    if original_file_element:
        original_file = original_file_element.parent.get("href")
    else:
        # If "Original file" link is not found, look for the "fullImageLink" div
        full_image_link = soup.find("div", class_="fullImageLink")
        if full_image_link:
            original_file = full_image_link.find("a").get("href")
        else:
            # If neither method works, return an error or a placeholder
            return "Image not found"

    # Append "https:" to the beginning of the URL if it starts with "//"
    if original_file.startswith("//"):
        original_file = "https:" + original_file

    # Get the file name and file type
    file_name = output_filename
    file_type = "." + original_file.split(".")[-1]

    output_file = f"{folder}/{file_name}{file_type}"

    # Download the file
    with open(output_file, "wb") as f:
        f.write(
            requests.get(
                original_file, headers=headers, stream=True, timeout=10
            ).content
        )

    return output_file


def get_image_metadata(base_url, image_page, user_agent):
    """Get the artist information about an image"""

    file_id = "File:" + image_page.split("File:")[-1]

    headers = {"User-Agent": user_agent}

    # get the image metadata
    image_metadata = requests.get(
        f"{base_url}w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&titles={file_id}&format=json",
        headers=headers,
        timeout=10,
    ).json()

    # Get artist information about the image, if it exists
    try:
        artist_with_html = image_metadata["query"]["pages"]["-1"]["imageinfo"][0][
            "extmetadata"
        ]["Artist"]["value"]
    except KeyError:
        artist_with_html = ""

    def remove_html_tags(text):
        """Remove html tags from a string"""
        import re

        clean = re.compile("<.*?>")
        return re.sub(clean, "", text)

    artist = html.unescape(remove_html_tags(artist_with_html)).strip()

    return artist


def get_article_soup(title):
    """Get the html of a Wikipedia page"""
    html = wikipedia.WikipediaPage(title).html()
    soup = BeautifulSoup(html, "html.parser")

    return soup


def get_infobox_or_first_image(base_url, soup, article, folder, user_agent):
    """Get the image from the infobox of a Wikipedia page, or if not present, the first image in the article, along with its caption"""
    # Attempt to find the first table with the class "infobox"
    infobox = soup.find("table", {"class": "infobox"})

    if infobox:
        # If an infobox is found, find the first image within it
        image_element = infobox.find("img")
        # Attempt to find the caption for the image within the infobox
        caption_element = infobox.find("div", class_="infobox-caption")

        image_caption = caption_element.text if caption_element else ""
    else:
        # If no infobox is found, find the first image in the article
        image_element = soup.find("img")
        caption_element = image_element.parent.find_next_sibling("figcaption")
        image_caption = caption_element.text if caption_element else ""

    # Go up one level to the link and get the href
    image_page = image_element.parent.get("href")

    image_file = download_image(base_url, image_page, "0", folder, user_agent)
    image_metadata = get_image_metadata(base_url, image_page, user_agent)
    image_info = {
        "image": image_file,
        "image_artist": image_metadata,
        "image_caption": image_caption,
        "image_page": image_page,
    }

    return [image_info]


def find_image_caption(base_url, image_page, language):
    """Find the image caption in the article"""

    # Use requests to download the HTML of a page
    html = requests.get(base_url + image_page, timeout=10).text

    soup = BeautifulSoup(html, "html.parser")
    # Find the div with the specified classes
    description_div = soup.find(
        "div", {"class": f"description mw-content-ltr {language}"}
    )
    if description_div:
        # Extract the text and split it by the colon ":"
        description_text = description_div.text.split(":", 1)
        # Check if the split was successful and there is text after the colon
        if len(description_text) > 1:
            # Save the text after the colon, stripping any leading or trailing whitespace
            image_caption = description_text[1].strip()
        else:
            # If there is no colon or no text after the colon, set image_caption to an empty string
            image_caption = ""
    else:
        # If the div is not found, set image_caption to an empty string
        image_caption = ""
    return image_caption


def main(article):
    """Get the images and their metadata from a Wikipedia article"""

    config = utils.load_config()
    folder = config["asset_folder"]
    user_agent = config["user_agent"]
    base_url = f"https://{config['wiki_language']}.wikipedia.org/"

    soup = get_article_soup(article)
    data = get_infobox_or_first_image(base_url, soup, article, folder, user_agent)

    full_text = soup.get_text()
    title = wikipedia.WikipediaPage(article).title
    for item in data:
        item["title"] = title
        item["full_text"] = full_text
        if item["image_caption"] == "":
            item["image_caption"] = find_image_caption(
                base_url, item["image_page"], config["wiki_language"]
            )

    utils.save_asset_file(data)
