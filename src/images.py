import cairosvg
from PIL import Image
import cv2
import utils

# This is because sometimes (rarely) Wikimedia Commons has a huge image
Image.MAX_IMAGE_PIXELS = None  # This removes the limit entirely


def convert_svg_to_png(image_path):
    """Converts an SVG image to PNG format with white background if it has transparency."""
    png_path = image_path.rsplit(".", 1)[0] + ".png"
    # Set the background to white (255, 255, 255) in case of transparency
    cairosvg.svg2png(
        url=image_path,
        write_to=png_path,
        parent_width=1080,
        parent_height=1080,
    )
    return png_path


def convert_to_jpg(image_path):
    """Converts an image to jpg format, ensuring any transparency is converted to white."""
    # Check if the image is an SVG and convert it to PNG first if it is
    if image_path.endswith(".svg"):
        image_path = convert_svg_to_png(image_path)

    img = Image.open(image_path)
    # If the image has an alpha channel, prepare to remove it by converting transparency to white
    if img.mode in ("RGBA", "LA") or (img.mode == "P" and "transparency" in img.info):
        # Create a white background image
        white_bg = Image.new("RGB", img.size, (255, 255, 255))
        # Paste the image onto the white background using the alpha channel as a mask
        white_bg.paste(
            img, mask=img.split()[3]
        )  # 3 is the index of alpha channel in RGBA
        rgb_img = white_bg
    else:
        rgb_img = img.convert("RGB")

    jpg_path = image_path.rsplit(".", 1)[0] + ".jpg"
    rgb_img.save(jpg_path, "JPEG")
    return jpg_path


def create_image(image_path):
    img = cv2.imread(image_path)
    original_height, original_width = img.shape[:2]

    target_size = (1080, 1920)

    scale = min(target_size[0] / original_width, target_size[1] / original_height)
    scaled_width = int(original_width * scale)
    scaled_height = int(original_height * scale)

    resized_img = cv2.resize(
        img, (scaled_width, scaled_height), interpolation=cv2.INTER_AREA
    )

    scale_bg = max(target_size[0] / original_width, target_size[1] / original_height)
    enlarged_width = int(original_width * scale_bg)
    enlarged_height = int(original_height * scale_bg)
    enlarged_img = cv2.resize(
        img, (enlarged_width, enlarged_height), interpolation=cv2.INTER_LINEAR
    )
    blurred_bg = cv2.GaussianBlur(enlarged_img, (0, 0), sigmaX=20, sigmaY=20)

    x_offset = (target_size[0] - scaled_width) // 2
    y_offset = (target_size[1] - scaled_height) // 2

    bg_x_offset = max(
        (enlarged_width - target_size[0]) // 2, 0
    )  # Ensure offset is not negative
    bg_y_offset = max(
        (enlarged_height - target_size[1]) // 2, 0
    )  # Ensure offset is not negative

    cropped_bg = blurred_bg[
        bg_y_offset : bg_y_offset + target_size[1],
        bg_x_offset : bg_x_offset + target_size[0],
    ]

    if (
        y_offset + scaled_height <= target_size[1]
    ):  # Ensure the resized image fits within the background
        cropped_bg[
            y_offset : y_offset + scaled_height, x_offset : x_offset + scaled_width
        ] = resized_img
    else:
        print("Error: Resized image does not fit within the background.")

    cv2.imwrite(image_path, cropped_bg)


def main():
    asset_list = utils.load_asset_file()

    # for each item in the json file
    for asset in asset_list:
        image_path = asset["image"]
        image_path = convert_to_jpg(image_path)
        asset["image"] = image_path
        utils.save_asset_file(asset_list)
        create_image(image_path)
