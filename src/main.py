"""
╔════════════════════════════════════════════════════════════════════╗
║                                                                    ║
║             ___         __  _      __        __                    ║░░
║            /   |  _____/ /_(_)____/ /__     / /_____               ║░░
║           / /| | / ___/ __/ / ___/ / _ \   / __/ __ \              ║░░
║          / ___ |/ /  / /_/ / /__/ /  __/  / /_/ /_/ /              ║░░
║         /_/  |_/_/   \__/_/\___/_/\___/   \__/\____/               ║░░
║                                                                    ║░░
║         _____ __               __     _    ___     __              ║░░
║        / ___// /_  ____  _____/ /_   | |  / (_)___/ /__  ____      ║░░
║        \__ \/ __ \/ __ \/ ___/ __/   | | / / / __  / _ \/ __ \     ║░░
║       ___/ / / / / /_/ / /  / /_     | |/ / / /_/ /  __/ /_/ /     ║░░
║      /____/_/ /_/\____/_/   \__/     |___/_/\__,_/\___/\____/      ║░░
║                                                                    ║░░
║                                                                    ║░░
╚════════════════════════════════════════════════════════════════════╝░░
  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
  ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

Article to Short Video generates a short video from a Wikipedia Article.
"""

import os
import shutil
import argparse

import utils
import wiki
import script
import audio
import images
import video
# import captions


def create_assets(folder_name: str) -> None:
    """Creates the assets folder"""

    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    else:
        shutil.rmtree(folder_name)
        os.makedirs(folder_name)


def parse_args():
    """Parses command-line arguments."""

    parser = argparse.ArgumentParser(
        description="Create a short video from a Wikipedia article."
    )

    parser.add_argument("article", type=str, help="The title of the Wikipedia article.")
    parser.add_argument(
        "--output", type=str, default="output.mp4", help="The name of the output file."
    )

    return parser.parse_args()


def main() -> None:
    """Creates a short video from a wikipedia article."""

    args = parse_args()

    article = args.article
    config = utils.load_config()
    asset_folder = config["asset_folder"]

    # Clean up the assets folder if exists
    if os.path.exists(asset_folder):
        shutil.rmtree(asset_folder)

    # Create the assets folder
    create_assets(asset_folder)

    # Extract the information from the wikipedia article
    wiki.main(article)

    # Write the script
    script.main()

    # Convert the script to speech
    audio.main()

    # Process the images
    images.main()

    # Create the video
    video.main(article)

    # Clean up the assets folder
    # shutil.rmtree(asset_folder)


if __name__ == "__main__":
    main()
