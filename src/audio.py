from pathlib import Path
from openai import OpenAI
from pydub import AudioSegment
from mutagen.mp3 import MP3
import utils

client = OpenAI()


def speed_up_audio(file_path, speed=1.2):
    """Speeds up the audio playback by a given factor and resaves with the same name."""
    audio = AudioSegment.from_file(file_path, format="mp3")
    audio_speed_up = audio.speedup(playback_speed=speed)
    audio_speed_up.export(file_path, format="mp3")


def main():
    """Generates a script for each asset in the asset list."""

    asset_list = utils.load_asset_file()
    config = utils.load_config()

    for asset in asset_list:
        # Define the speech file path to be in the assets folder with the name "0.mp3"
        speech_file_path = Path(__file__).parent / "assets" / "0.mp3"
        response = client.audio.speech.create(
            model="tts-1-hd",
            voice="alloy",
            input=asset["script"],
        )

        # Stream the response to the file
        response.stream_to_file(speech_file_path)

        speed_up_audio(speech_file_path, speed=config["speech_speed"])

        # Add the file name to the asset
        asset["speech_file"] = str(speech_file_path)

        # Use mutagen to load the audio file and extract its duration
        audio = MP3(speech_file_path)
        duration = audio.info.length  # Duration in seconds

        # Add the duration to the asset dictionary
        asset["speech_duration"] = duration

        # Save the updated asset list
        utils.save_asset_file(asset_list)
