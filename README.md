# Article to Short Video

## Overview
Article to Short Video is a Python library designed to automate the creation of short videos from Wikipedia articles. This tool extracts information from a specified Wikipedia article, generates a script, converts the script into speech, processes images, and finally assembles everything into a video format. It's an excellent way to transform textual content into a more engaging, visual format.

## Features
- **Article Extraction**: Automatically fetches content from Wikipedia articles.
- **Script Generation**: Converts article content into a script for video narration.
- **Text-to-Speech**: Transforms the generated script into speech audio.
- **Image Processing**: Handles image extraction and processing for video slides.
- **Video Assembly**: Combines audio, images, and text into a cohesive video.
- **Customizable Output**: Offers configuration options for video properties like FPS, zoom speed, and resolution.

## Installation
To install the necessary dependencies for Article to Short Video, run the following command in your terminal:

```
pip install -r requirements.txt
```

## Getting Started

1. Adjust the configuration options in the `config.py` file to your liking. This is where you can set the output video resolution, FPS, zoom speed, etc.
2. Create a `.env` file with your OpenAI key. Follow the `.env.example` file as an example.
3. Run the `main.py` script to generate a short video from a Wikipedia article. The only argument is the article name.

```
python main.py "California"
```

Videos are saved to the videos folder.

